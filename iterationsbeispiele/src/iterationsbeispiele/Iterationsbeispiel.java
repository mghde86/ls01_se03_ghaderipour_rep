package iterationsbeispiele;

import java.util.Scanner;

public class Iterationsbeispiel {

	public static void main(String[] args) {

		Scanner tastatur = new Scanner(System.in);

		printNNumbers("Geben Sie eine Zahl ein:", tastatur);
		

	}

	public static void printNNumbers(String text, Scanner myScanner) {
		System.out.println(text);
		int n = myScanner.nextInt();
		int zaehler = 1;
		int minusEins = n;
		while (zaehler <= n) {
			if (zaehler == n) {
				minusEins--;
				System.out.print(minusEins);
			} else {
				minusEins--;
				System.out.print(minusEins + ",");
			}

			zaehler++;
		}
	}

}
