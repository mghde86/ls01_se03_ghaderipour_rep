package lotto;

import java.util.*;

public class Lotto {

	public static void main(String[] args) {
		// Eingabe
		int[] lotto = new int[7];
		int zahl = (int) (Math.random() * 49 + 1);
		System.out.println("Deine Lottowahl ist = 3, 7, 12, 18, 37, 42");

		// Bearbeitung = Lottozufallszahlen generieren
		for (int i = 1; i < lotto.length; i++) {
			lotto[i] = (int) (Math.random() * 49 + 1);

			// Bearbeitung = wenn Zahl gleich der Zfz is, noch mal zuf�llig w�hlen
			for (int j = 1; j < i; j++) {
				if (lotto[i] == lotto[j])
					j--;
			}
		}

		// Array sortieren
		Arrays.sort(lotto);

		// Ausgabe = Zufallszahlen ausgeben
		for (int k = 1; k < lotto.length; k++) {
			System.out.println("Die heutigen Lottozahlen sind:[" + lotto[k] + "] ");

		}

	}

}
