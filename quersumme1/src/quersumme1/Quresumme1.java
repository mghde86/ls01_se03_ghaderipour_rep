package quersumme1;
import java.util.*;
 
public class Quresumme1 {
	public static void main(String[] args) {
		
		System.out.print("Geben Sie bitte eine Zahl ein :");
		Scanner tastatur = new Scanner(System.in);
		int zahl1= tastatur.nextInt();
		
		System.out.print("Quersumme ist: "+ berechneQuersumme(zahl1));
		
	}
     
    public static int berechneQuersumme(int zahl) {
        int summe = 0;
        while(zahl >0) {
            summe = summe + (zahl % 10);
            zahl = zahl / 10;
        }
        return summe;
    }
 
}
