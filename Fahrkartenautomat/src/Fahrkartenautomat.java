import java.util.Scanner;

class Fahrkartenautomat {

	public static double fahrkartenbestellungErfassen() {
		int anzahlTickets = 0;
		double ticketPreis = 0;
		Scanner tastatur = new Scanner(System.in);
		System.out.print("W�hlen Sie ihre Wunschfahrkarte f�r Berlin AB aus:\r\n"
				+ "  Einzelfahrschein Regeltarif AB [2,90 EUR] (1)\r\n"
				+ "  Tageskarte Regeltarif AB [8,60 EUR] (2)\r\n"
				+ "  Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)\n");

		System.out.print("Ihr wahl: ");
		double tastatur1 = tastatur.nextDouble();
		if (tastatur1 == 1) {
			ticketPreis = 2.90;
			System.out.print("Anzahl der Tickets: ");
			anzahlTickets = tastatur.nextInt();
			ticketPreis = ticketPreis * anzahlTickets;

		} else if (tastatur1 == 2) {
			ticketPreis = 8.60;
			System.out.print("Anzahl der Tickets: ");
			anzahlTickets = tastatur.nextInt();
			ticketPreis = ticketPreis * anzahlTickets;

		} else if (tastatur1 == 3) {
			ticketPreis = 23.50;
			System.out.print("Anzahl der Tickets: ");
			anzahlTickets = tastatur.nextInt();
			ticketPreis = ticketPreis * anzahlTickets;
		} else {
			System.out.println(" >>falsche Eingabe<< ");

		}
		return ticketPreis;
	}

	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		double eingeworfenemuenze;
		double eingezahlterGesamtbetrag = 0.0;
		Scanner tastatur = new Scanner(System.in);

		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.format("Noch zu zahlen: %4.2f � %n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
			eingeworfenemuenze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfenemuenze;
		}
		return eingezahlterGesamtbetrag - zuZahlenderBetrag;
	}

	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");
	}

	public static void rueckgeldAusgeben(double rueckgabebetrag) {

		if (rueckgabebetrag > 0.0) {
			System.out.format("Der R�ckgabebetrag in H�he von %4.2f � %n", rueckgabebetrag);
			System.out.println("wird in folgenden M�nzen ausgezahlt:");

			while (rueckgabebetrag >= 2.0) {// 2 EURO-muenzen
				System.out.println("2 EURO");
				rueckgabebetrag -= 2.0;
			}
			while (rueckgabebetrag >= 1.0) {// 1 EURO-muenzen
				System.out.println("1 EURO");
				rueckgabebetrag -= 1.0;
			}
			while (rueckgabebetrag >= 0.5) // 50 CENT-muenzen
			{
				System.out.println("50 CENT");
				rueckgabebetrag -= 0.5;
			}
			while (rueckgabebetrag >= 0.2) // 20 CENT-muenzen
			{
				System.out.println("20 CENT");
				rueckgabebetrag -= 0.2;
			}
			while (rueckgabebetrag >= 0.1) // 10 CENT-M�zen
			{
				System.out.println("10 CENT");
				rueckgabebetrag -= 0.1;
			}
			while (rueckgabebetrag >= 0.05)// 5 CENT-muenzen
			{
				System.out.println("5 CENT");
				rueckgabebetrag -= 0.05;
			}
		}
	}

	public static void main(String[] args) {

		double zuZahlenderBetrag;
		double rueckgabebetrag;

		// Swith case Aufgabe 3

		while (true) {
			zuZahlenderBetrag = fahrkartenbestellungErfassen();
			rueckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
			fahrkartenAusgeben();
			rueckgeldAusgeben(rueckgabebetrag);

			System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
					+ "Wir w�nschen Ihnen eine gute Fahrt.");
			System.out.println(
					"-----------------------------------------------------------------------------------------\n \n ");
		}
	}
}