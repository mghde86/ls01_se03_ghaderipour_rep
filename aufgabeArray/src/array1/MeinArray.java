package array1;

import java.util.Scanner;

public class MeinArray {

	public static void main(String[] args) {
		aufgabe1();
		aufgabe2();
		while (true) {
			aufgabe3();
		}
	}

	// AUFGABE 1
	public static void aufgabe1() {

		System.out.println("Aufgabe 1");

		int[] liste0 = new int[10];
		int x = 0;

		// AUFGABE
		for (int i = 0; i < 10; i++) {
			liste0[i] = x;
			x = x + 1;
			// System.out.printf(x+ "|" +i+"\n");
		}
		// Ausgabe
		for (int i = 0; i < liste0.length; i++) {
			System.out.print(liste0[i] + " ");
		}
		System.out.println();
	}

//------------------------------------------------------------------------------------------
	// AUFGABE 2
	public static void aufgabe2() {
		System.out.println("Aufgabe 2");

		int[] liste1 = new int[10];
		int y = 1;

		// AUFGABE
		for (int i = 0; i < 10; i++) {
			liste1[i] = y;
			y = y + 2;
			// System.out.printf(y+ "|" +i+"\n");
		}
		// Ausgabe
		for (int i = 0; i < liste1.length; i++) {
			System.out.print(liste1[i] + " ");
		}
		System.out.println();
	}

//--------------------------------------------------------------------------------------		  
	// Aufgabe 3
	public static void aufgabe3() {

		System.out.println("Aufgabe 3");

		Scanner input = new Scanner(System.in);
		char[] liste2 = new char[5];

		// AUFGABE
		for (int i = 0; i < 5; i++) {
			System.out.println("Geben sie Zahl " + (i + 1) + " ein :");
			liste2[i] = input.next().charAt(0);
			System.out.println(liste2[i]);
			// Ausgabe
		}
		for (int i = 0; i < liste2.length; i++) {
			System.out.print(liste2[i] + " ");
		}
		System.out.println();
		// Ausgabe R�CKW�RTS bzw. Umgekehrt

		for (int i = liste2.length - 1; i >= 0; i = i - 1) {
			System.out.print(liste2[i] + " ");
		}
		System.out.println();
	}

}
