﻿import java.util.function.DoubleToLongFunction;

/** Variablen.java
    Ergaenzen Sie nach jedem Kommentar jeweils den Quellcode.
    @author
    @version
 */
public class Variablen {
	public static void main(String [] args){
		/* 1. Ein Zaehler soll die Programmdurchlaeufe zaehlen.
          Vereinbaren Sie eine geeignete Variable */
		int zaehler = 1234567890;


		/* 2. Weisen Sie dem Zaehler den Wert 25 zu
          und geben Sie ihn auf dem Bildschirm aus.*/
		zaehler = 25;
		System.out.println("Zaehler den Wert="+zaehler);
		/* 3. Durch die Eingabe eines Buchstabens soll der Menuepunkt
          eines Programms ausgewaehlt werden.
          Vereinbaren Sie eine geeignete Variable */
		char symbol = 'A';


		/* 4. Weisen Sie dem Buchstaben den Wert 'C' zu
          und geben Sie ihn auf dem Bildschirm aus.*/
		symbol = 'C';
		System.out.println("Buchstaben den Wert= "+symbol);
		/* 5. Fuer eine genaue astronomische Berechnung sind grosse Zahlenwerte
          notwendig.
          Vereinbaren Sie eine geeignete Variable */
		long grossZahlen = 100000000000l;


		/* 6. Weisen Sie der Zahl den Wert der Lichtgeschwindigkeit zu
          und geben Sie sie auf dem Bildschirm aus.*/
		grossZahlen = 299792l;
		System.out.println("Wert der Lichtgeschwindigkeit="+grossZahlen);

		/* 7. Sieben Personen gruenden einen Verein. Fuer eine Vereinsverwaltung
          soll die Anzahl der Mitglieder erfasst werden.
          Vereinbaren Sie eine geeignete Variable und initialisieren sie
          diese sinnvoll.*/
		int verein = 7;

		/* 8. Geben Sie die Anzahl der Mitglieder auf dem Bildschirm aus.*/
		System.out.println("Anzahl der Mitglieder="+verein);

		/* 9. Fuer eine Berechnung wird die elektrische Elementarladung benoetigt.
          Vereinbaren Sie eine geeignete Variable und geben Sie sie auf
          dem Bildschirm aus.*/
		double Elementarladung = 0.0;
		
		System.out.println("");

		/*10. Ein Buchhaltungsprogramm soll festhalten, ob eine Zahlung erfolgt ist.
          Vereinbaren Sie eine geeignete Variable. */
		boolean Buchhaltungsprogramm=true;
		
		/*11. Die Zahlung ist erfolgt.
          Weisen Sie der Variable den entsprechenden Wert zu
          und geben Sie die Variable auf dem Bildschirm aus.*/
		System.out.println("Zahlung erfolgt ist="+Buchhaltungsprogramm);
		
	}//main
}// Variablen