public class Aufagab2 {

	public static void main(String[] args) {
		System.out.printf("0!%5s%19s%4s\n","=","=",1);
		System.out.printf("1!%5s%2s%17s%4s\n","=",1,"=",1);
		System.out.printf("2!%5s%6s%13s%4s\n","=","1 * 2","=",2);
		System.out.printf("3!%5s%10s%9s%4s\n","=","1 * 2 * 3","=",6);
		System.out.printf("4!%5s%14s%5s%4s\n","=","1 * 2 * 3 * 4","=",24);
		System.out.printf("5!%5s%18s%1s%4s\n","=","1 * 2 * 3 * 4 * 5","=",120);
	}

}




